/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todoclist;
import java.util.*;
import java.util.ArrayList;

/**
 *
 * @author traumaxp
 */

public class ToDoClist {

    public static void main(String[] args) {
        test();
    }
    public static void test() {
        Scanner scan = new Scanner(System.in);
        boolean quit = false;
        ArrayList<String> todoclist = new ArrayList<String>();
        do{
            System.out.println(" Command ('h' for help): \n");
            String cmd = scan.nextLine();
            System.out.print(todoclist);
            switch (cmd) {
                case "a":
                case "append":
                    System.out.print("Enter an item: ");
                    Command.append(scan, todoclist);
                    break;
                case "i":
                case "insert":
                   Command.insert(scan, todoclist);
                    break;
                case "s":
                case "swap":
                   Command.swap(scan, todoclist);
                    break;
                case "d":
                case "delete":
                    Command.delete(scan, todoclist);
                    break;

                case "h" :
                case "help":
                    Command.help();
                    break;
                case "quit":
                    System.out.print("quitting");
                    quit = true;
            }
        }while(quit == false);
        scan.close();
    }}