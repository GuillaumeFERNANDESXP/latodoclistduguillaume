
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todoclist;
import java.lang.reflect.Array;
import java.util.*;
/**
 *
 * @author traumaxp
 */


public class Command {

    ArrayList<String> todoclist = new ArrayList<String>();

    public static String ScannerString() {

        Scanner scan = new Scanner(System.in);
        String item = scan.nextLine();
        return item;
    }
    public static int ScannerInt(){
        Scanner scan = new Scanner(System.in);
        int index = scan.nextInt();
        return index;
    }

    public static void append(Scanner scan, ArrayList<String> todoclist) {
        todoclist.add(ScannerString());
        System.out.println("Current To-Do List (xx items(s) on yy)");
        for (int i = 0; i < todoclist.size(); i++)
            System.out.println(i+1 + " -> " + todoclist.get(i));
    }

    public static void insert(Scanner scan, ArrayList<String> todoclist){
        System.out.println("Enter where you want to insert item");
        int index = scan.nextInt();
        System.out.println("what do you want to add");
        String itemForInsert = scan.nextLine();
        todoclist.add(index, ScannerString());
        System.out.println(itemForInsert +"add at the index " + index);
        for (int i = 0; i < todoclist.size(); i++)
            System.out.println(i+1 + " -> " + todoclist.get(i));
    }

    public static void swap(Scanner scan, ArrayList<String> todoclist){

    System.out.print("Select first item");
    int numberForSwap1 = scan.nextInt();
    System.out.print("Select second item");
    int numberForSwap2 = scan.nextInt();
    Collections.swap(todoclist, numberForSwap1, numberForSwap2);
    System.out.println(todoclist);
    System.out.print("Swapped elements " + (numberForSwap1) + " and "+(numberForSwap2)+ ")");
}

    public static void delete(Scanner scan , ArrayList<String> todoclist) {

        do {
            System.out.print("Choose the item to delete ( between xx and yy): ");
            int number = scan.nextInt();
            todoclist.remove(number);
            System.out.print("Item deleted");
        } while (todoclist.size() > 0);
    }
    public static void help() {
        System.out.println("List of commands: \n" + "\t append|a \t Add a new item to the To-Do List \n"
                + "\t insert|i \t Insert a new item at a given index \n" + "\t swap|s \t Swap two items \n"
                + "\t delete|d \t Delete an item \n" + "\t help|h \t Print this help \n"
                + "\t quit|q \t Quit todoclist\n");

    }

}

